# skt.sh

This is a proof-of-concept of converting [skt] into a bash script. Much of
the work done by skt involves calling various shell commands and this could
be greatly simplified by a complete conversion to bash scripts.

For usage information:

* `skt.sh --help`
* `skt.sh merge --help`
* `skt.sh build --help`

[skt]: https://github.com/cki-project/skt
