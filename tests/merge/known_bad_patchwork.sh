#!/bin/bash

./skt.sh merge --workdir=$WORKDIR --fetch-depth=1 --ref=v4.19.15 \
    --baserepo=https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git \
    --pw=https://patchwork.ozlabs.org/patch/1023062/

if [ $? -ne 1 ]; then
    exit 1
else
    exit 0
fi
